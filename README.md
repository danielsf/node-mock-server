# Node Mock Server

A very tiny mock server for NodeJS backed applications

## Installing

Download the repository:

    git clone https://github.com/danieldsf/node-mock-server.git

Install the dependencies:

    npm install

## Running

Run with node:

    node main.js


