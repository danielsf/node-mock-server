// Database:
const Datastore = require('nedb');

let db = {};

db.public = new Datastore('./storage/public.db');
db.secret = new Datastore('./storage/secret.db');

// You need to load each database (here we do it asynchronously)
db.public.loadDatabase();
db.secret.loadDatabase();

// Validator
const datalize = require('datalize');

const field = datalize.field;

const jsonServer = require('json-server');

const bodyParser = require('body-parser');

const multer = require('multer'); // v1.0.5

const upload = multer(); // for parsing multipart/form-data

// Returns an Express server, a Json server and a Socket.io handler
const server = jsonServer.create();

server.use(bodyParser.json()); // for parsing application/json

server.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

const serverHttp = require('http').Server(server);

const io = require('socket.io')(serverHttp);

// Set default middlewares (logger, static, cors and no-cache)
server.use(jsonServer.defaults());

// Add custom HTTP routes
server.get('/custom', function (req, res) { res.json({ msg: 'hello' }) })

// Add custom WS routes
io.on('connection', function (socket) {
  socket.emit('news', { hello: 'world' });
  
  socket.on('my other event', function (data) {
    console.log(data);
  });
});

// Returns an Express router
const router = jsonServer.router('./storage/db.json');

server.use(router);

server.listen(3000);
